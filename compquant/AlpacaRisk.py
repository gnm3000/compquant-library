import os
from compquant.Restdata import RESTData

from alpaca_trade_api.rest import REST
from sympy.solvers import solve
from sympy import Symbol
import pandas as pd

class AlpacaRisk():
    api = None
    leverage=None
    def __init__(self,account_id=None):
        db_provider = RESTData()
        acc = db_provider.getAccount("HdNRkTDqrzOIEvk")
        os.environ["APCA_API_KEY_ID"] = acc["config"]["api_key"]
        os.environ["APCA_API_SECRET_KEY"] = acc["config"]["secret_key"]
        os.environ["APCA_API_BASE_URL"] = acc["config"]["endpoint"]
        self.api = REST()
        pass

    def existPosition(self,symbol):
        positions = self.getPositions()
        for p in positions:
            if(symbol==p["symbol"]):
                return True
        return False
    def getOrders(self):
        #positions = self.api.list_positions()
        #symbols = [x.symbol for x in positions]
        #order_df = pd.DataFrame()
        order_list = self.api.list_orders(status='all', limit=25)
        order_list=[x.__dict__["_raw"] for x in order_list]
        df=pd.DataFrame(order_list)
        df=df[(df.status=="new") | (df.status=="held")]
        df.created_at = pd.to_datetime(df.created_at)
        return df.sort_values(by="created_at",ascending=False).to_dict("records")
        #return order_list

    def getPositions(self):
        positions = self.api.list_positions()
        positions_list = []
        for o in positions:
            positions_list.append(o.__dict__["_raw"])
        return positions_list
    def setLeverage(self,leverage):
        self.leverage=leverage
    def getAccount(self):
        return self.api.get_account().__dict__["_raw"]

    def close_position(self,ticker,amount=None):
        order=None
        if(not self.existPosition(symbol=ticker)):
            print("error. No existe una posicion con dicho ticker")
            return
        position_ticker = self.api.get_position(symbol=ticker)
        
        position_ticker.qty=int(position_ticker.qty)
        
        
        if((amount==None) or (amount==position_ticker.qty)):
            orders=[x for x in self.api.list_orders(status="open") if (x.symbol==ticker)]
            for o in orders:
                self.api.cancel_order(order_id=o.id)
            self.api.close_position(symbol=ticker)
            return True
        else:
            amount=int(amount)
            if(amount>position_ticker.qty):
                print("Intenta liquidar mas de lo que hay")
                return "Error"
        if(position_ticker.qty<0):
            if(amount==None):
                qty=abs(int(position_ticker.qty))
            else:
                qty=abs(int(amount))
            print("SHORT")
            side="buy"
        if(position_ticker.qty>0):
            print("LONG")
            side="sell"
            if(amount==None):
                qty=abs(int(position_ticker.qty))
            else:
                qty=abs(int(amount))
        order=self.api.submit_order(ticker, qty=qty, side=side, type="market", time_in_force="day")
        return order.__dict__["_raw"]
        
        # ACA HAY QUE CERRAR O REDUCIR EL MONTO DE LA POSICION DEL TICKER!!!!!!!!!!!
        

    def getAmount(self,ticker,side="buy",risk_per_trade_at_stop=0.01,stoploss_percent=0.01,max_percent_portfolio=0.1,day_trading=False):
        # Si lo maximo que puedo perder es 1%. Y el Stop esta a 5% de distancia
        # Que cantidad de shares debo comprar?
        last_price = self.getLastQuote(symbol=ticker).askprice
        #last_price=119.9
        account=self.getAccount()
        leverage=int(account["multiplier"])
        
        account["cash"]=float(account["cash"])
        total_leveraged = account["cash"]*leverage
        if(day_trading==True):
            total_leveraged= float(account["daytrading_buying_power"])
        else:
            total_leveraged= float(account["regt_buying_power"])
        
        entry = last_price
        stop = last_price-(last_price*stoploss_percent)
        amount = Symbol("amount")
        amount_to_invest=(solve(amount-((amount/entry)*stop)-(risk_per_trade_at_stop*total_leveraged)))
        amount_to_invest=amount_to_invest[0]
        print("amount_to_invest",amount_to_invest)
        hypotesis_position_percent = amount_to_invest/total_leveraged
        print(hypotesis_position_percent,max_percent_portfolio)
        if(hypotesis_position_percent>max_percent_portfolio):
            amount_to_invest=max_percent_portfolio*total_leveraged

        shares= int(amount_to_invest/last_price)
        print("ticker %s last_price %s amount_leveraged=%s shares=%s" % (ticker,last_price,amount_to_invest,shares))
        return int(shares)


    def getLastQuote(self,symbol):
        return self.api.get_last_quote(symbol=symbol)
    def buy(self,ticker,amount,stoploss_percent,reward=2):
        ask_price = self.getLastQuote(symbol=ticker).askprice
        stoploss_price= ask_price-(ask_price*stoploss_percent)
        take_profit_price = ask_price+(ask_price*stoploss_percent*reward)
        
        self.api.submit_order(symbol=ticker,side="buy",type="market",qty=int(amount),time_in_force='day',order_class='bracket',
                                take_profit=dict(
                                    limit_price=str(round(take_profit_price,2)),
                                ),
                                stop_loss=dict(
                                    stop_price=str(round(stoploss_price,2)),
                                    limit_price=str(round(stoploss_price-0.5,2)),
                                ))
        
    def sell(self,ticker,amount,stoploss_percent,reward=2):
        ask_price = self.getLastQuote(symbol=ticker).ask_price
        stoploss_price= ask_price+(ask_price*stoploss_percent)
        take_profit_price = ask_price-(ask_price*stoploss_percent*reward)

        self.api.submit_order(symbol=ticker,side="sell",type="market",qty=int(amount),time_in_force='day',order_class='bracket',
                                take_profit=dict(
                                    limit_price=str(round(take_profit_price,2)),
                                ),
                                stop_loss=dict(
                                    stop_price=str(round(stoploss_price,2)),
                                    limit_price=str(round(stoploss_price,2)),
                                ))

"""AR= AlpacaRisk()
print(AR.close_position(ticker="AAPL"))"""
#print(AR.getAccount())
#AR.buy(ticker="AAPL",amount=AR.getAmount(ticker="AAPL",side="buy",risk_per_trade_at_stop=0.01,stoploss_percent=0.1),stoploss_percent=0.01,reward=2)
#AR.sell(ticker="AAPL",amount=AR.getAmount(ticker="AAPL",side="buy",risk_per_trade_at_stop=0.01,stoploss_percent=0.1),stoploss_percent=0.01,reward=2)


# Puedo hacer long y short, con Risk MM. 
