import pandas as pd
from binance.client import Client
import time,os
from .utils import upload_file
from .Restdata import RESTData
#from utils import upload_file
#from Restdata import RESTData

from alpaca_trade_api.rest import REST
from yahoofinancials import YahooFinancials
import datetime
from datetime import timedelta
import requests
class Data():

    def save(self,file_,name_file,ticker):
        a=upload_file(file_,name_file,ticker)
        print(a)
        return a["secure_url"]

    client_binance_spot=None
    client_alpaca_api=None
    def __init__(self,broker="binance"):
        g_api_key="p3ID3pBqoSee8J6up7atAl4hTAxfUZnXKXJvRKI8IIGTJWiiaAgNFsPcfz09bzDx"
        g_secret_key="DoEMks4iCSBiefiowYlA07EmGqEdY9s2SJRcKoxE6wCsgDJVFaN6VFyhcRN68ZOQ"
        
        self.client_binance_spot = Client(g_api_key, g_secret_key)
        if(broker=="alpaca"):
            rest_api = RESTData()
            acc=rest_api.getAccount("HdNRkTDqrzOIEvk")
            acc["config"]
            os.environ["APCA_API_KEY_ID"] = acc["config"]["api_key"]
            os.environ["APCA_API_SECRET_KEY"] = acc["config"]["secret_key"]
            os.environ["APCA_API_BASE_URL"] = acc["config"]["endpoint"]
            os.environ["APCA_API_DATA_URL"] = "https://data.alpaca.markets"
            self.client_alpaca_api = REST()
        pass


    def getNasdaq100(self,reconstruir_data=True):
        """Este es para el portfolio de Equity Nasdaq100 con HRP
        Hay que agregar a esto persistencia Quizas en Firebase
        """
        def download_ticker_alpaca(ticker):
            data=[]
            for i in range(20):
                a=(datetime.datetime.today() - timedelta(days=90*i)).strftime('%Y-%m-%d')
                df__=(self.client_alpaca_api.get_aggs(ticker, timespan="day", multiplier=1, _from="2017-01-01", to=a).df)
                if(i==0):
                    print(ticker)
                    print(df__)
                    if(len(df__)==0):
                        break
                data.append(df__)
            return pd.concat(data).drop_duplicates().sort_index()
        #data_nasdaq["AAPL"] = download_ticker_alpaca("AAPL")


        def normalize_ticker(ticker,df_prices):
            yahoo_financials = YahooFinancials(ticker)
            yf_data=yahoo_financials.get_historical_price_data(start_date="2017-01-01",end_date=datetime.datetime.today().strftime('%Y-%m-%d'),time_interval="daily")
            
            def normalize_by_splits(df_prices,ticker,yf_data):
                print(yf_data[ticker]["eventsData"].keys())
                if("splits" in yf_data[ticker]["eventsData"].keys()):
                    for key in yf_data[ticker]["eventsData"]["splits"].keys():
                        from_date = yf_data[ticker]["eventsData"]["splits"][key]["formatted_date"]
                        numerator = yf_data[ticker]["eventsData"]["splits"][key]["numerator"]
                        denominator = yf_data[ticker]["eventsData"]["splits"][key]["denominator"]
                        print("from: %s numerator: %s deniminator: %s" % (from_date,numerator,denominator))
                        
                        df_prices.loc[from_date:]=df_prices[from_date:]*(numerator/denominator) 
                        
                return df_prices
            return normalize_by_splits(df_prices,ticker,yf_data) 
        if(reconstruir_data==True):
            req = requests.get("https://raw.githubusercontent.com/dariusk/corpora/master/data/corporations/nasdaq.json")
            nasdaq_tickers=pd.json_normalize(req.json(),"corporations")
            list_nasdaq_symbols=list(nasdaq_tickers.symbol.values)
            list_nasdaq_symbols=list(set(list_nasdaq_symbols)-set(["ESRX","MYL","CELG","VIAB","DTV"]))
            data_nasdaq={}
            cont=0
            for ticker in list_nasdaq_symbols:
                print(ticker)
                data_nasdaq[ticker] = download_ticker_alpaca(ticker)
                data_nasdaq[ticker]=normalize_ticker(ticker,data_nasdaq[ticker].close)
                cont=cont+1
                print("ticker %s %s / %s " % (ticker,cont,len(list_nasdaq_symbols)))
                time.sleep(1)
            #pd.DataFrame.from_dict(data_nasdaq,orient='index').T
            for ticker in list_nasdaq_symbols:
                if(len(data_nasdaq[ticker])==0):
                    del data_nasdaq[ticker]
            list_nasdaq_symbols=list(data_nasdaq.keys())
            df_data_nasdaq=pd.DataFrame.from_dict(data_nasdaq,orient='index').T
            df_data_nasdaq.to_csv("/tmp/df_data_nasdaq.csv")
            self.save("/tmp/df_data_nasdaq.csv","df_data_nasdaq.csv","nasdaq100")
            
        
        pass
    def get_pandas_prices(self,klines):
            df_prices=pd.DataFrame(klines,columns=["openTime","open","high","low","close","volume","closeTime","quote_asset_volume",
                                    "number_of_trades","taker_buy_base_asset_volume","taker_buy_quote_asset_volume","."])
            df_prices.openTime=pd.to_datetime(df_prices.openTime,unit="ms",utc=True)
            df_prices.closeTime=pd.to_datetime(df_prices.closeTime,unit="ms",utc=True)

            df_prices=df_prices.set_index("openTime")
            df_prices=df_prices.drop(columns=["."])
            return df_prices
    def update_data(self,all_symbols):
        count=0
        rd=RESTData()
        for symbol in all_symbols:
            data_symbol=rd.getData(symbol)
            file_url = data_symbol["url"]
            print("file_url",file_url)
            df_prices_original = pd.read_csv(file_url,parse_dates=["openTime"],index_col=["openTime"])
            start_date=df_prices_original.index[-1].strftime('%Y-%m-%d %H:%M:%S')
            klines = self.client_binance_spot.get_historical_klines(symbol, Client.KLINE_INTERVAL_1DAY, start_date)
            df_prices=self.get_pandas_prices(klines)
            df_ = pd.concat([df_prices_original,df_prices])
            df_ = df_[~df_.index.duplicated(keep='last')]
        
            df_.to_csv("/tmp/total_binance_price_%s.csv" % symbol)
            self.save("/tmp/total_binance_price_%s.csv" % symbol,"total_binance_price_%s.csv" % symbol,symbol)
            count=count+1
            print(symbol,count,"/",len(all_symbols))
            time.sleep(1)
            
    def download_data(self,all_symbols):
        count=0
        
        for symbol in all_symbols:
            print("download_data",symbol)
            klines = self.client_binance_spot.get_historical_klines(symbol, Client.KLINE_INTERVAL_1DAY, "1 Jan, 2019")
            df_prices=self.get_pandas_prices(klines)
            df_prices.to_csv("/tmp/total_binance_price_%s.csv" % symbol)
            self.save("/tmp/total_binance_price_%s.csv" % symbol,"total_binance_price_%s.csv" % symbol,symbol)
            
            count=count+1
            print(symbol,count,"/",len(all_symbols))
            time.sleep(0.6)
    def getDataPortfolioCrypto(self):
        rd=RESTData()
        data_=rd.getData("crypto_portfolio_binance")
        print("Get crypto_portfolio_binance. last update:",data_["updated_at"])
        return pd.read_csv(data_["url"],parse_dates=["openTime"],index_col=["openTime"])
    def getDataPortfolioNasdaq100(self):
        rd=RESTData()

        return pd.read_csv(rd.getData("nasdaq100")["url"],parse_dates=[0],index_col=[0])
        
    def updateDataPortfolioCrypto(self,download=False,all_symbols=[]):
        """Este es para el portfolio crypto
        """
        

        if(len(all_symbols)==0):
            all_symbols=['BTCUSDT','ETHUSDT','BNBUSDT','NEOUSDT','LTCUSDT','QTUMUSDT','ADAUSDT','XRPUSDT','EOSUSDT','IOTAUSDT','XLMUSDT','ONTUSDT','TRXUSDT','ETCUSDT','ICXUSDT','NULSUSDT','VETUSDT','LINKUSDT','WAVESUSDT','BTTUSDT','ONGUSDT','HOTUSDT','ZILUSDT','ZRXUSDT','FETUSDT','BATUSDT','XMRUSDT','ZECUSDT','IOSTUSDT','CELRUSDT','DASHUSDT','NANOUSDT','OMGUSDT','THETAUSDT','ENJUSDT','MITHUSDT','MATICUSDT','ATOMUSDT','TFUELUSDT','ONEUSDT','FTMUSDT','ALGOUSDT','GTOUSDT','DOGEUSDT','DUSKUSDT','ANKRUSDT','WINUSDT','COSUSDT','MTLUSDT','TOMOUSDT','PERLUSDT','DENTUSDT','MFTUSDT','KEYUSDT','DOCKUSDT','WANUSDT','FUNUSDT','CVCUSDT','CHZUSDT','BANDUSDT','BEAMUSDT','XTZUSDT','RENUSDT','RVNUSDT','HBARUSDT','NKNUSDT','STXUSDT','KAVAUSDT','ARPAUSDT','IOTXUSDT','RLCUSDT','CTXCUSDT','BCHUSDT','TROYUSDT','VITEUSDT','FTTUSDT']
        if(download==True):
            self.download_data(all_symbols)
        else:
            self.update_data(all_symbols)
        all_close_prices={}
        all_prices={}
        rd=RESTData()
        for symbol in all_symbols:
            #symbol="CKBUSDT"
            
            data_symbol=rd.getData(symbol)
            file_url = data_symbol["url"]
            print("file_url",file_url)
            df_prices = pd.read_csv(file_url,parse_dates=["openTime"],index_col=["openTime"])   
            all_close_prices[symbol]=df_prices.close
            all_prices[symbol]=df_prices
        
        df_close_prices=pd.DataFrame.from_dict(all_close_prices)
        last_row=df_close_prices.iloc[-1].reset_index()
        last_row.columns=["ticker","last_price"]
        last_row=last_row.dropna()
        symbols_survive=last_row.ticker.values
        df_close_prices=df_close_prices[symbols_survive]
        df_close_prices = df_close_prices[~df_close_prices.index.duplicated(keep='last')]
        df_close_prices.to_csv("/tmp/df_all_close_prices_binance_spot.csv")
        print(self.save("/tmp/df_all_close_prices_binance_spot.csv","df_all_close_prices_binance_spot.csv","crypto_portfolio_binance"))


"""d=Data(broker="alpaca")
print(d.getNasdaq100(reconstruir_data=False))
print(d.getNasdaq100(reconstruir_data=True))
a=(d.getDataPortfolioNasdaq100())
print(a)
AA=set(list(a.columns))
BB=set(list(a.iloc[-1].dropna().index))
print(AA-BB)
#print("====")"""
#
# print(d.getDataPortfolioCrypto())