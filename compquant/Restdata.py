import requests
import random
import datetime
import string
class RESTData():
    base_url = "https://api-rest.compquant.com"
    def __init__(self):
        pass
    def generate_random_id(self):
        # printing letters
        letters = string.ascii_letters
        return ''.join(random.choice(letters) for i in range(15))
    def setBaseUrl(self,url):
        if(not "http" in url):
            print("error. la URL debe tener http")
            return False
        self.base_url=url
    
    def addData(self,data):
        data.update({"updated_at":datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')})
        if(not "id" in data):
            data.update({"id":self.generate_random_id()})
        req=requests.post( "%s/%s" % (self.base_url,"data/add"),json=data )
        print(req.content)
        if(req.status_code==200):

            return True
        else:
            print("ERROR")
            return
    
    def addStrategy(self,data):
        data.update({"id":self.generate_random_id()})
        req=requests.post( "%s/%s" % (self.base_url,"strategy/add"),json=data )
        print(req.content)
        if(req.status_code==200):

            return True
        else:
            print("ERROR")
            return
    def addAccount(self,data):
        data.update({"id":self.generate_random_id()})
        req=requests.post( "%s/%s" % (self.base_url,"account/add"),json=data )
        print(req.content)
        if(req.status_code==200):

            return True
        else:
            print("ERROR")
            return
    def deleteData(self,data_id):
        req=requests.get( "%s/%s" % (self.base_url,"data/delete?id=%s" % (data_id)))
        return (req.json())
    
    def deleteStrategy(self,strategy_id):
        accounts = self.getAccounts()
        for acc in accounts:

            self.delete_strategy_to_account(strategy_id,acc["id"])

        req=requests.get( "%s/%s" % (self.base_url,"strategy/delete?id=%s" % (strategy_id)))
        return (req.json())
    def get_api_data(self):
        return {"strategies":self.getStrategies(),"accounts":self.getAccounts()}
    def getStrategies(self):
        req=requests.get( "%s/%s" % (self.base_url,"strategy/list"))
        return (req.json())
    def getDatas(self):
        req=requests.get( "%s/%s" % (self.base_url,"data/list"))
        return (req.json())
        
    def getAccounts(self):
        req=requests.get( "%s/%s" % (self.base_url,"account/list"))
        return (req.json())
    def getAccount(self,id):
        req=requests.get( "%s/%s" % (self.base_url,"account/list?id=%s" % id))
        return (req.json())
    def getData(self,id):
        req=requests.get( "%s/%s" % (self.base_url,"data/list?id=%s" % id))
        return (req.json())
    
    def getStrategy(self,id):
        req=requests.get( "%s/%s" % (self.base_url,"strategy/list?id=%s" % id))
        return (req.json())
    def updateStrategy(self,data):
        req=requests.post( "%s/%s" % (self.base_url,"strategy/update"),json=data)
        return (req.json())
    def updateAccount(self,data):
        req=requests.post( "%s/%s" % (self.base_url,"account/update"),json=data)
        return (req.json())
    
        
    def updateData(self,data):
        data.update({"updated_at":datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')})
        req=requests.post( "%s/%s" % (self.base_url,"data/update"),json=data)
        return (req.json())
    
    
    def delete_all(self):
        req=requests.get( "%s/%s" % (self.base_url,"delete_all"))
        return (req.json())

    def getStrategyFromAccount(self,strategy_id,account_id):
        account = self.getAccount(account_id)
        if(account):
          if("strategies" in account):
            for s in account["strategies"]:
              if(s["strategy_id"]==strategy_id):
                return True
            return False
        else:
          print("No existe la cuenta")
          return False
    def add_strategy_to_account(self, strategy_id,account_id):
        account=self.getAccount(account_id)
        if(account):
            print("Si account")
            if(self.getStrategyFromAccount(strategy_id,account_id)==True):
                print("La estrategia ya existe en la cuenta")
                return
            strategy = self.getStrategy(strategy_id)
            now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')

            s={"strategy_id":strategy_id,"updated_at":now, "start_date":now,"name":strategy["name"]}
            if("strategies" in account):
                print("si strategies")
                account["strategies"].append(s)
            else:
                print("no stategies")
                account["strategies"]=[]
                account["strategies"].append(s)
            req=requests.post( "%s/%s" % (self.base_url,"account/update"),json=account )
            print(req.content)
            if(req.status_code==200):
                print(req.json())
                return
            else:
                print("ERROR")
                return
        else:
              print("no existe la cuenta")
              return
    def deleteAccount(self,account_id):
        req=requests.get( "%s/%s" % (self.base_url,"account/delete?id=%s" % (account_id)))
        return (req.json())

    def delete_strategy_to_account(self, strategy_id,account_id):
        account=self.getAccount(account_id)
        if(account):
            if(self.getStrategyFromAccount(strategy_id,account_id)!=True):
                print("La estrategia no existe en la cuenta")
                return
            strategy = self.getStrategy(strategy_id)
            now = datetime.datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
            new_strategies_account=[]
            strategies_account = account["strategies"]
            for s in strategies_account:
                if(s["strategy_id"]!=strategy_id):
                    new_strategies_account.append(s)
            account["strategies"] = new_strategies_account
            req=requests.post( "%s/%s" % (self.base_url,"account/update"),json=account )
            print(req.content)
            if(req.status_code==200):

                return True
            else:
                print("ERROR")
                return
        else:
            print("no existe la cuenta")
            return
