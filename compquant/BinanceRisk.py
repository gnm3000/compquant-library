from sympy.solvers import solve
from sympy import Symbol
from binance_f import RequestClient
from binance_f.model import *
from binance_f.constant.test import *
from binance_f.base.printobject import *
import requests as req
import time

def get_qty_minimos():
    qty_minimos={}
    qty_minimos["ETHUSDT"] = 0.001
    qty_minimos["EOSUSDT"] = 0.1
    qty_minimos["BTCUSDT"] = 0.001
    qty_minimos["LTCUSDT"] = 0.001
    qty_minimos["ETCUSDT"] = 0.01
    qty_minimos["BNBUSDT"] = 0.01
    qty_minimos["LINKUSDT"] = 0.01
    qty_minimos["BCHUSDT"] = 0.001
    qty_minimos["XRPUSDT"] = 0.1
    qty_minimos["TRXUSDT"] = 1
    qty_minimos["XLMUSDT"] = 1
    qty_minimos["ADAUSDT"] = 1
    qty_minimos["XMRUSDT"] = 0.001
    qty_minimos["DASHUSDT"] = 0.001
    qty_minimos["ZECUSDT"] = 0.001
    qty_minimos["XTZUSDT"] = 0.1
    qty_minimos["XTZUSDT"] = 0.1
    qty_minimos["ATOMUSDT"] = 0.01
    qty_minimos["IOTAUSDT"] = 0.1
    qty_minimos["BATUSDT"] = 0.1
    qty_minimos["VETUSDT"] = 1
    qty_minimos["ONTUSDT"] = 0.1
    qty_minimos["NEOUSDT"] = 0.01
    qty_minimos["QTUMUSDT"] = 0.1
    qty_minimos["IOSTUSDT"] = 1
    qty_minimos["SUSHIUSDT"] = 1
    

    price_minimos={}
    price_minimos["ETHUSDT"] = 0.01
    price_minimos["EOSUSDT"] = 0.001
    price_minimos["BTCUSDT"] = 0.01
    price_minimos["LTCUSDT"] = 0.01
    price_minimos["ETCUSDT"] = 0.001
    price_minimos["BNBUSDT"] = 0.001
    price_minimos["LINKUSDT"] = 0.001
    price_minimos["BCHUSDT"] = 0.01
    price_minimos["XRPUSDT"] = 0.0001
    price_minimos["TRXUSDT"] = 0.00001
    price_minimos["XLMUSDT"] = 0.00001
    price_minimos["ADAUSDT"] = 0.00001
    price_minimos["XMRUSDT"] = 0.01
    price_minimos["DASHUSDT"] = 0.01
    price_minimos["ZECUSDT"] = 0.01
    price_minimos["XTZUSDT"] = 0.001
    price_minimos["ATOMUSDT"] = 0.001
    price_minimos["IOTAUSDT"] = 0.0001
    price_minimos["BATUSDT"] = 0.0001
    price_minimos["VETUSDT"] = 0.000001
    price_minimos["ONTUSDT"] = 0.0001
    price_minimos["NEOUSDT"] = 0.001
    price_minimos["QTUMUSDT"] = 0.001
    price_minimos["IOSTUSDT"] = 0.000001
    price_minimos["SUSHIUSDT"] = 0.0001
    

    return qty_minimos,price_minimos

qty_minimos,price_minimos = get_qty_minimos()

def formatter_number(field="price",symbol=None,number=None):
    number=float(number)

    import math

    def step_size_to_precision(ss):
        return max(ss.find('1'), 1) - 1

    def format_value(val, step_size):
        digits = step_size_to_precision(step_size)
        val *= 10 ** digits
        return '{1:.{0}f}'.format(digits, math.floor(val) / 10 ** digits)

    qty_minimos,price_minimos = get_qty_minimos()
    if(field=="price"):
        qty_minimos=price_minimos
    else:
        pass
    #symbol = "VETUSDT"
    step_size = str(qty_minimos[symbol])
    #quantity = 0.00725562
    #print("quantity",number)
    #print(format_value(number, step_size))
    return format_value(number, step_size)
#formatter_number("BTCUSDT",0.00775562)


class BinanceOrdersClass():
    request_client=None
    def __init__(self):
        g_api_key="p3ID3pBqoSee8J6up7atAl4hTAxfUZnXKXJvRKI8IIGTJWiiaAgNFsPcfz09bzDx"
        g_secret_key="DoEMks4iCSBiefiowYlA07EmGqEdY9s2SJRcKoxE6wCsgDJVFaN6VFyhcRN68ZOQ"
        self.request_client = RequestClient(api_key=g_api_key, secret_key=g_secret_key)
    def formatter_number(self,field="price",symbol=None,number=None):
        return formatter_number(field=field,symbol=symbol,number=number)

          
    def change_leverage(self,ticker,leverage):
        try:
            self.request_client.change_margin_type(symbol=ticker,marginType=FuturesMarginType.CROSSED)
        except:
            pass
        try:
            self.request_client.change_initial_leverage(symbol=ticker, leverage=int(leverage))
        except:
            pass
    def close_position(self,ticker,positionSide,amount):
        amount=formatter_number(field="qty",symbol=ticker,number=float(amount))
        if(positionSide=="LONG"):
            result2 = self.request_client.post_order(symbol=ticker,
                                                    side=OrderSide.SELL,
                                                    positionSide="LONG",
                                                    ordertype=OrderType.MARKET,
                                                    quantity=amount)
            self.request_client.cancel_all_orders(symbol=ticker)
        else:
            result2 = self.request_client.post_order(symbol=ticker,
                                                    side=OrderSide.BUY,
                                                    positionSide="SHORT",
                                                    ordertype=OrderType.MARKET,
                                                    quantity=amount)
            self.request_client.cancel_all_orders(symbol=ticker)

        return result2
    def order_buy(self,ticker,qty=None):
        
        if(qty==None):
            qty=qty_minimos[ticker]
        qty = formatter_number(field="qty",symbol=ticker,number=qty)
        result1 = self.request_client.post_order(symbol=ticker,side=OrderSide.BUY, positionSide="LONG",ordertype=OrderType.MARKET, quantity=qty)
        price = self.getPrice(symbol=ticker)
        stopPrice = price-(price*0.01)
        stopPrice=formatter_number(field="price",symbol=ticker,number=stopPrice)
        result2 = self.request_client.post_order(symbol=ticker,side=OrderSide.SELL,positionSide="LONG",ordertype=OrderType.STOP_MARKET,stopPrice=stopPrice,closePosition=True)
        TakeprofitPrice=price+2*(price*0.01)
        TakeprofitPrice=formatter_number(field="price",symbol=ticker,number=TakeprofitPrice)
        result3 = self.request_client.post_order(symbol=ticker,side=OrderSide.SELL,positionSide="LONG",ordertype=OrderType.TAKE_PROFIT_MARKET,stopPrice=TakeprofitPrice,closePosition=True)
        return result1,result2,result3

    def order_sell(self,ticker,qty=None,stop=None):
        if(qty==None):
            print("ERROR! order_sell")
            qty=qty_minimos[ticker]
        qty = formatter_number(field="qty",symbol=ticker,number=qty)
        result1 = self.request_client.post_order(symbol=ticker,side=OrderSide.SELL, positionSide="SHORT",ordertype=OrderType.MARKET, quantity=qty)
        price = self.getPrice(symbol=ticker)
        stopPrice = price+(price*0.01)
        stopPrice=formatter_number(field="price",symbol=ticker,number=stopPrice)
        result2 = self.request_client.post_order(symbol=ticker,side=OrderSide.BUY,positionSide="SHORT",ordertype=OrderType.STOP_MARKET,stopPrice=stopPrice,closePosition=True)
        TakeprofitPrice=price-2*(price*0.01)
        TakeprofitPrice=formatter_number(field="price",symbol=ticker,number=TakeprofitPrice)
        result3 = self.request_client.post_order(symbol=ticker,side=OrderSide.BUY,positionSide="SHORT",ordertype=OrderType.TAKE_PROFIT_MARKET,stopPrice=TakeprofitPrice,closePosition=True)
        return result1,result2,result3




    def get_information_account(self):
        result= self.request_client.get_account_information()
        
        account={
        'canDeposit':result.canDeposit, 
        'canTrade':result.canTrade,  
        'canWithdraw':result.canWithdraw,  
        'feeTier':result.feeTier, 
        'maxWithdrawAmount':result.maxWithdrawAmount,  
        'totalInitialMargin':result.totalInitialMargin,  
        'totalMaintMargin':result.totalMaintMargin, 
        'totalMarginBalance':result.totalMarginBalance, 
        'totalOpenOrderInitialMargin':result.totalOpenOrderInitialMargin,  
        'totalPositionInitialMargin':result.totalPositionInitialMargin, 
        'totalUnrealizedProfit':result.totalUnrealizedProfit,  
        'totalWalletBalance':result.totalWalletBalance, 
        'updateTime':result.updateTime}
            
        print("========================================================")
        print("========================================================")
        assets=[]
        for a in result.assets:
            
            if(a.marginBalance!=0):
                asset ={'asset':a.asset, 'initialMargin':a.initialMargin,
                'maintMargin':a.maintMargin, 'marginBalance':a.marginBalance, 
                'maxWithdrawAmount':a.maxWithdrawAmount, 'openOrderInitialMargin':a.openOrderInitialMargin,
                'positionInitialMargin':a.positionInitialMargin, 'unrealizedProfit':a.unrealizedProfit, 
                'walletBalance':a.walletBalance}
                assets.append(asset)

        positions=[]
        """for p in result.positions:
            if(p.positionInitialMargin!=0):
                positions.append({"initialMargin":p.initialMargin,"isolated":p.isolated,"leverage":p.leverage,
                                    "maintMargin":p.maintMargin,"openOrderInitialMargin":p.openOrderInitialMargin,
                                    "positionInitialMargin":p.positionInitialMargin,"positionSide":p.positionSide,"symbol":p.symbol,
                                    "unrealizedProfit":p.unrealizedProfit})"""
        all_positions=[x.__dict__ for x in self.request_client.get_position() if x.positionAmt!=0]
        return account,assets,all_positions
    def balance(self):
        result_balance = self.request_client.get_balance()
                
        total_account = 0
        for r in result_balance:
            total_account = total_account + r.balance
        return total_account
    def getPrice(self,symbol=""):
        result=self.request_client.get_symbol_price_ticker(symbol=symbol)[0]
        return float(result.price)
    def getOrders(self):
        all_tickers_with_positions=[x.__dict__["symbol"] for x in self.request_client.get_position() if x.positionAmt!=0]
        orders=[]
        for ticker in all_tickers_with_positions:
            result=self.request_client.get_all_orders(symbol=ticker)
            
            for r in result:
                if(r.status=="FILLED"):
                    continue
                if(r.status=="CANCELED"):
                    continue
                orders.append({"status":r.status,"avgPrice":r.avgPrice,"closePosition":r.closePosition,"executedQty":r.executedQty,
                "orderId":r.orderId,"origQty":r.origQty,"origType":r.origType,"positionSide":r.positionSide,"price":r.price,
                "reduceOnly":r.reduceOnly,"side":r.side,"symbol":r.symbol,"stopPrice":r.stopPrice,"timeInForce":r.timeInForce,"type":r.type,
                "updateTime":r.updateTime})

        return orders

    def calculate(self,ticker="",side="buy",percent_where_stop=0.05,leverage=10,risk_per_trade_at_stop=0.01):
        print("total cuenta all assets USD",self.balance())
        account = self.balance()
        #leverage=10
        entry=self.getPrice(symbol=ticker)
        stop=entry-(entry*percent_where_stop)
        total_leveraged = account*leverage
        amount = Symbol("amount")
        amount_to_invest=(solve(amount-((amount/entry)*stop)-(risk_per_trade_at_stop*total_leveraged)))
        return formatter_number(field="qty",symbol=ticker,number=float(amount_to_invest[0])/entry)
        


"""a=BinanceOrdersClass()
b=(a.getOrders())
print(len(b))
print(b)"""
"""a=BinanceOrdersClass()
print(a.balance())
account,assets,positions=a.get_information_account()


print("positions",positions)
print(a.calculate(ticker="SUSHIUSDT"))"""

"""ticker="SUSHIUSDT"
a=BinanceOrdersClass()
account,assets,positions=a.get_information_account()

print(account)
stop_loss=0.01
a.change_leverage(ticker=ticker,leverage=10)
qty=a.calculate(ticker=ticker,side="buy",percent_where_stop=float(stop_loss),leverage=10,risk_per_trade_at_stop=0.001)
print(qty)
print(a.order_sell(ticker="SUSHIUSDT",qty=1))"""