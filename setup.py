from setuptools import setup

setup(name='compquant',
      version='0.1.1',
      description='CompQuant Official Library',
      packages=['compquant'],
      install_requires=["matplotlib","requests","python-binance","pandas","cloudinary","alpaca-trade-api","yahoofinancials"],
      author_email='gnm3000@gmail.com',
      zip_safe=False)
